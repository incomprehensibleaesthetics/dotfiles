#!/usr/bin/env bash

# Abort on error.
set -e

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"
XDG_DATA_HOME="${XDG_DATA_HOME:-$HOME/.local/share}"

function _link {
  local NAME="$1"
  local TARGET_PATH="$2"
  local SOURCE_PATH="$SCRIPT_DIR/$3"

  local TARGET_DIR=$(target_dir "$TARGET_PATH" "$SOURCE_PATH")

  # Return if the path is already linked correctly.
  if [ -L "$TARGET_PATH" ]; then
    if [ "$(readlink -f "$TARGET_PATH")" = "$SOURCE_PATH" ]; then
      return 0
    fi
  fi

  # Remove interfering files.
  if [ -e "$TARGET_PATH" ]; then
    echo ">>> Backing up $NAME..."
    mv -iv "$TARGET_PATH" "$TARGET_PATH.$(date +%F_%H-%M).bak"
  elif [ -L "$TARGET_PATH" ]; then
    # It's a symbolic link but it's not pointing where it should.
    rm "$TARGET_PATH"
  fi

  # Create target location (with intermediate directories) if necessary.
  if [ ! -d "$TARGET_DIR" ]; then
    echo ">>> Creating $TARGET_DIR..."
    mkdir -p "$TARGET_DIR"
  fi

  echo ">>> Linking $NAME..."
  ln -s "$SOURCE_PATH" "$TARGET_PATH"

  return 0
}

function target_dir {
  local TARGET_PATH="$1"
  local SOURCE_PATH="$SCRIPT_DIR/$2"

  if [ -d "$SOURCE_PATH" ]; then
    echo "$TARGET_PATH"
  else
    echo "$(dirname "$TARGET_PATH")"
  fi
}


## PREPARE ###################################################################

mkdir -p "$XDG_CONFIG_HOME"
mkdir -p "$XDG_DATA_HOME"


## git #######################################################################

echo ">>> Configuring git..."

if [ ! -e "$HOME/.gitconfig" -a ! -e "$XDG_CONFIG_HOME/git/config" ]; then
  mkdir -p "$XDG_CONFIG_HOME/git"
  touch "$XDG_CONFIG_HOME/git/config"
elif [ -e "$HOME/.gitconfig" -a ! -e "$XDG_CONFIG_HOME/git/config" ]; then
  echo ">>> Moving git config to preferred location: $HOME/.gitconfig -> $XDG_CONFIG_HOME/git/config"
  mkdir -p "$XDG_CONFIG_HOME/git"
  mv "$HOME/.gitconfig" "$XDG_CONFIG_HOME/git/config"
elif [ -e "$HOME/.gitconfig" -a -e "$XDG_CONFIG_HOME/git/config" ]; then
  echo ">>> WARNING! Found git config at both '$HOME/.gitconfig' and '$XDG_CONFIG_HOME/git/config'..." >&2
fi

# Global .gitignore
#
# TODO: Ensure this doesn't replace a potential previous configuration.
git config --global core.excludesfile "$DOTFILE_PATH/global-gitignore"

# Fancier diffs
if command -v delta &> /dev/null; then
  git config --global core.pager "delta --syntax-theme base16"
  git config --global interactive.diffFilter "delta --color-only"
  git config --global delta.features "side-by-side line-numbers decorations"
  git config --global delta.whitespace-error-style "22 reverse"
  git config --global delta.decorations.commit-decoration-style "bold yellow box ul"
  git config --global delta.decorations.file-style "bold yellow ul"
  git config --global delta.decorations.file-decoration-style "none"
fi

git config --global merge.tool meld

git config --global mergetool.keepBackup false


## khal ######################################################################

_link "khal config" "$XDG_CONFIG_HOME/khal" "khal"


## khard #####################################################################

_link "khard config" "$XDG_CONFIG_HOME/khard" "khard"


## neomutt ###################################################################

_link "neomutt config" "$XDG_CONFIG_HOME/neomutt" "neomutt"


## nix #######################################################################

_link "nix config" "$XDG_CONFIG_HOME/nixpkgs" "nix"


## todo-txt ##################################################################

_link "todo-txt config" "$HOME/.todo" "todo"


## tmux ######################################################################

_link "tmux config" "$HOME/.tmux.conf" "tmux.conf"


## vdirsyncer ################################################################

_link "vdirsyncer config" "$XDG_CONFIG_HOME/vdirsyncer" "vdirsyncer"


## xdg ########################################################################

_link "xdg user dirs config" "$XDG_CONFIG_HOME/user-dirs.dirs" "user-dirs.dirs"
_link "xdg user dirs locale config" "$XDG_CONFIG_HOME/user-dirs.locale" "user-dirs.locale"


## other #####################################################################

_link "binaries" "$HOME/bin" "bin"

if command -v home-manager &> /dev/null; then
  home-manager switch
else
  echo "Warning! nix home-manager is not installed. Some config will be missing." >&2
fi
