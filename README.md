# iooioio's Dotfiles

These are my dotfiles. They are currently a frankenstein of homebrew
shell-script-and-symlink config files combined with home-manager nix
expressions. I hope to migrate everything over to home-manager eventually.


## Setup

Run the following to get started:

```sh
./setup.sh
```

This will set up symlinks to the relevant paths (making backups in the process),
run `home-manager sync` and perform a few manual config commands (e.g. for
configuring git).


## Applying Changes / Updates

### Short Version

Re-run the setup script whenever you fetch new updates or make changes to files
from the repo. It is idempotent so you can run it as often as you like if you
want to be certain all changes were picked up.

### Longer Version

Technically not all changes require re-running the setup script. Files that are
managed by nix home-manager _will_. They are read-only and require editing the
respective nix expresions and rebuilding to apply modifications. However,
manually symlinked files can be edited directly and their changes are effective
immediately.

To keep things simple, I still recommend simply rebuilding all the time anyway.
It is relatively fast and should have no undesired side-effects. Furthermore, I
intend to migrate everything to nix home-manager anyway so rebuilding will be
necessary in the future anyway.