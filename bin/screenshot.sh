#!/usr/bin/env bash

set -eo pipefail

TARGET_DIR="$HOME/inbox/screenshots/$(hostname)"
MESSAGE="Screenshot taken"
SCROT_OPTS=""

if [ "$1" = "window" ]; then
  MESSAGE="Screenshot taken (window only)"
  SCROT_OPTS="$SCROT_OPTS -u"
fi

mkdir -p "$TARGET_DIR"
scrot $SCROT_OPTS '%Y-%m-%d_%H-%M-%S-screenshot.png' -e "mv \$f $TARGET_DIR/"
dunstify "$MESSAGE"
