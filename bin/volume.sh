#!/usr/bin/env bash

# You can call this script like this:
#
# $ ./volume.sh up
# $ ./volume.sh down
# $ ./volume.sh mute
# $ ./volume.sh --all-sinks max
# $ ./volume.sh --all-sources max

function get_volume {
    pulsemixer --get-volume | cut -d ' ' -f 1
}

function is_muted {
    pulsemixer --get-mute
}

function send_notification {
    volume=$(get_volume)

    dunstify \
      --hints="int:value:$volume" \
      --icon=audio-volume-high \
      --replace=2593 \
      --urgency=normal \
      "Volume"
}

case $1 in
    up)
        pulsemixer --unmute
        pulsemixer --change-volume +5
        send_notification
        ;;
    down)
        pulsemixer --unmute
        pulsemixer --change-volume -5
        send_notification
        ;;
    mute)
        pulsemixer --toggle-mute
        if [[ "$(is_muted)" = "1" ]]; then
            dunstify -i audio-volume-muted -r 2593 -u normal "Mute"
        else
            send_notification
        fi
        ;;
    --all-sinks)
        if [[ "$2" = "max" ]]; then
          pulsemixer --list-sinks | grep -oP '(?<=ID: )[^,]+' | xargs -I {} pulsemixer --id {} --set-volume 100
        fi
        ;;
    --all-sources)
        if [[ "$2" = "max" ]]; then
          pulsemixer --list-sources | grep -oP '(?<=ID: )[^,]+' | xargs -I {} pulsemixer --id {} --set-volume 100
        fi
        ;;
esac
