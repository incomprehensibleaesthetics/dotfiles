#!/usr/bin/env bash

set -euo pipefail

if systemctl --user is-active --quiet polybar-background; then
  systemctl --user stop polybar-background
else
  systemctl --user start 'polybar-background'

  # Restart the other bars, to ensure the background bar doesn't end up on top.
  systemctl --user restart polybar-clock
  systemctl --user restart polybar-workspaces
fi
