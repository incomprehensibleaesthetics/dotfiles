{ config, pkgs, ... }:

{
  programs.lazygit = {
    enable = true;

    settings = {
      git = {
        paging = {
          colorArg = "always";
          pager = "delta --paging=never --syntax-theme=base16";
        };       
      };

      gui = {
        theme = {
          activeBorderColor = [ "blue" ];
          selectedLineBgColor = [ "reverse" ];
          selectedRangerBgColor = [ "reverse" ];
        };

        showCommandLog = false;
        showFileTree = false;
      };

      os = {
        editPreset = "helix";
      };
    };
  };
}
