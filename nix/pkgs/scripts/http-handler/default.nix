{ pkgs, stdenv, ... }:

with pkgs;

stdenv.mkDerivation rec {
  pname = "http-handler";
  version = "1.0.0";

  src = ./http-handler.sh;

  dontUnpack = true;

  installPhase = ''
    runHook preInstall

    mkdir -p "$out/bin/"
    cp "$src" "$out/bin/http-handler"

    runHook postInstall
  '';

  meta = {
    description = "Opens a link with the web browser or a dedicated app, depending on the url";
  };
}
