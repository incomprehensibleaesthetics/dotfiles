#!/usr/bin/env bash

set -euo pipefail


## Functions ################################################################

function rewrite_teams_url() {
  echo "$1" | sed 's/^https\?:/msteams:/'
}


## Entry Point ##############################################################

INPUT_URL="$1"
BROWSER=${BROWSER:-librewolf}

case "$INPUT_URL" in
  http://teams.microsoft.com/*)
    exec teams "$(rewrite_teams_url "$INPUT_URL")"
    ;;

  https://teams.microsoft.com/*)
    exec teams "$(rewrite_teams_url "$INPUT_URL")"
    ;;

  *)
    exec "$BROWSER" "$INPUT_URL"
    ;;
esac
