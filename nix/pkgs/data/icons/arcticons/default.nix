{ lib, stdenvNoCC, fetchFromGitHub, gtk3, hicolor-icon-theme }:

# TODO: Make it possible to choose white or black theme.
# TODO: Make it possible to choose line thickness.

stdenvNoCC.mkDerivation {
  pname = "arcticons";
  version = "2022-09-29-f3d67fdc3";

  src = fetchFromGitHub {
    repo = "Arcticons";
    owner = "jtrees";
    rev = "79cd2e7dcb4ccfea4d7293c453507a261c00a777";
    sha256 = "sha256-tlYpG05XYTiXPnvY2Dny1PpmgTzSktHq1tiRlx+ikLc=";
  };

  nativeBuildInputs = [ gtk3 ];

  propagatedBuildInputs = [ hicolor-icon-theme ];

  dontDropIconThemeCache = true;

  # These fixup steps are slow and unnecessary.
  dontPatchELF = true;
  dontRewriteSymlinks = true;

  buildPhase = ''
    runHook preBuild

    cd freedesktop-theme
    patchShebangs generate.sh
    ./generate.sh white 1.2

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    mkdir -p $out/share/icons
    cp -r arcticons $out/share/icons

    runHook postInstall
  '';

  meta = with lib; {
    description = "Line-based icon pack forked from Frost";
    homepage = "https://github.com/jtrees/Arcticons";
    license = licenses.gpl3Only;
    platforms = platforms.unix;
    maintainers = with maintainers; [ jtrees ];
  };
}
