args@{ config, lib, pkgs, ... }:

with lib;
with attrsets;

let
  cfg = attrByPath [ "nixosConfig" "myConfig" "email" ] { enable = false; } args;
in {
  config = mkIf cfg.enable {
    xdg.configFile.mailcap.source = ./mailcap;

    # Used for viewing html emails in neomutt.
    home.file.".w3m/config".source = ./w3m_config;

    services.mbsync = {
      enable = true;
      configFile = config.home.homeDirectory + "/.config/mbsync/mbsyncrc";
      postExec = "${pkgs.notmuch}/bin/notmuch new";
    };

    # TODO: Configure mbysnc with nix.
    xdg.configFile."mbsync/mbsyncrc".source = ./mbsyncrc;

    # TODO: Configure notmuch with nix.
  };
}
