{ lib, stdenvNoCC, fetchFromGitHub, }:

stdenvNoCC.mkDerivation {
  pname = "kitty-cappuccin-theme";
  version = "2022-09-27-8c6d97e4";

  src = fetchFromGitHub {
    repo = "kitty";
    owner = "catppuccin";
    rev = "8c6d97e4f064951e2eabed1560b3b0ecd0d684d2";
    sha256 = "sha256-ikIh2pGBdw6q6zGK9HV/ZRIA6PL02hbKbwcnaBGQChg=";
  };

  # These fixup steps are slow and unnecessary.
  dontPatchELF = true;
  dontRewriteSymlinks = true;

  installPhase = ''
    runHook preInstall

    mkdir "$out"
    cp *.conf "$out/"

    runHook postInstall
  '';
}
