{ config, lib, pkgs, ... }:

let
  # "frappe", "latte", "macchiato" or "mocha"
  flavor = config.myConfig.theme.colorSchemeFlavor;

  themes = pkgs.callPackage ./catppuccin-themes.nix {};
in {
  programs.kitty = {
    settings = {
      ## Borders #############################################################

      window_padding_width = 20;

      single_window_margin_width = 2;
      window_margin_width = 0;
      window_border_width = "2px";
      draw_minimal_borders = "no";


      ## Fonts ###############################################################

      font_family      = "JetBrains Mono";
      bold_font        = "auto";
      italic_font      = "auto";
      bold_italic_font = "auto";


      ## Tabs ################################################################

      tab_bar_edge = "top";

      tab_bar_style = "separator";
      tab_activity_symbol = "!";

      # This one doesn't translate the whitespace properly so it's in
      # `extraConfig` instead.
      #
      # tab_separator = " | ";

      active_tab_font_style = "normal";


      ## Other ###############################################################

      allow_hyperlinks = "ask";
      scrollback_pager_history_size = "1"; # MB
    };

    extraConfig = ''
      tab_separator " | "

      ## Theme ###############################################################

      include ${flavor}.conf

      # TODO: Keep in sync with flavor.
      tab_bar_background      none
      inactive_tab_background #24273a
      inactive_tab_foreground #5b6078
      active_tab_background   #24273a
      active_tab_foreground   #cad3f5
    '';

    keybindings = {
      "kitty_mod+h" = "neighboring_window left";
      "kitty_mod+j" = "neighboring_window down";
      "kitty_mod+k" = "neighboring_window up";
      "kitty_mod+l" = "neighboring_window right";

      # Remap this combo because we just overrode the default (kitty_mod+l) above.
      "kitty_mod+#" = "next_layout";
    };
  };

  xdg.configFile = lib.attrsets.optionalAttrs config.programs.kitty.enable {
    "kitty/${flavor}" = {
      source = "${themes}/${flavor}.conf";
      target = "kitty/${flavor}.conf";
    };
  };
}
