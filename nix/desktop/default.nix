{ config, lib, pkgs, ... }:

with lib;

let

  cfg = config.myConfig.desktop;

  myPkgs = {
    arcticons = (pkgs.callPackage ../pkgs/data/icons/arcticons {});
    http-handler = (pkgs.callPackage ../pkgs/scripts/http-handler {});
  };

in {

  imports = [
    ./discord
    ./dunst
    ./feh.nix
    ./librewolf
    ./kitty
    ./mpv.nix
    ./openmw.nix
    ./polybar
    ./rofi
  ];


  ## Interface ###############################################################

  options = {
    myConfig.desktop = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Whether or not to enable a graphical desktop";
      };

      cursorTheme.name = mkOption {
        type = types.str;
        example = "posy-improved-cursor";
        description = ''
          Name of the icon theme to use.
        '';
      };

      cursorTheme.package = mkOption {
        type = types.package;
        description = ''
          The package path that contains the cursor theme given in the name option.
        '';
      };
    };
  };


  ## Implementation ##########################################################

  config = mkIf cfg.enable {

    home.packages = with myPkgs; [
      arcticons
      http-handler
    ];

    gtk.cursorTheme = with cfg.cursorTheme; {
      inherit package name;
    };

    # Detailed config is in kitty/default.nix.
    programs.kitty.enable = true;

    programs.openmw = {
      enable = true;

      # TODO: Add derivation for game data.
      mwDataPath = "/home/josh/resources/games/morrowind/original-game/app/Data Files";

      intro.enabled = false;

      # TODO: Add derivation for each mod.
      additionalConfig = {
        data = [
          "/home/josh/resources/games/morrowind/mods/patch-for-purists"
          "/home/josh/resources/games/morrowind/mods/umopp-3.1.0"
          "/home/josh/resources/games/morrowind/mods/truetype-fonts"
        ];
        content = [
          "Patch for Purists.esm"
          "Patch for Purists - Book Typos.ESP"
          "Patch for Purists - Semi-Purist Fixes.ESP"
          "Unofficial Morrowind Official Plugins Patched.ESP"
        ];
      };
    };

    xdg.desktopEntries = {
      cmus = {
        name = "cmus";
        genericName = "Music Player";
        exec = "cmus";
        icon = "cmus";
        terminal = true;
        type = "Application";
        categories = [ "Audio" "AudioVideo" "Player" ];
        settings = { Keywords = "music;audio;player;"; };
      };
    };

  };
}
