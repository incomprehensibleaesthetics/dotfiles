{
  home.file = {
    "librewolf/userChrome" = {
      source = ./userChrome.css;

      # TODO: Don't hard-code the profile dir.
      target = ".librewolf/dyzza4mf.default/chrome/userChrome.css";
    };

    "librewolf/userContent" = {
      source = ./userContent.css;

      # TODO: Don't hard-code the profile dir.
      target = ".librewolf/dyzza4mf.default/chrome/userContent.css";
    };
  };
}
