{ config, pkgs, ... }:

{
  programs.mpv = {
    enable = true;

    config = {
      script-opts = "ytdl_hook-ytdl_path=yt-dlp";
    };
  };
}
