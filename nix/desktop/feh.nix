{ config, pkgs, ... }:

{
  xdg.configFile = {
    "feh/themes" = {
      text = "feh --auto-rotate --scale-down --image-bg black";
    };
  };
}