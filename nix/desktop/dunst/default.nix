{ config, pkgs, ... }:

let
  myPkgs = {
    arcticons = (pkgs.callPackage ../../pkgs/data/icons/arcticons {});
  };
in {
  services.dunst = {
    enable = true;

    iconTheme = {
      name = "arcticons";
      package = myPkgs.arcticons;
      size = "32x32";
    };

    settings = {
      global = {
        monitor = 0;
        follow = "mouse";

        geometry = "350x5-20+48";
        progress_bar_height = 1;
        progress_bar_max_width = 320;
        progress_bar_frame_width = 0;
        indicate_hidden = "yes";
        shrink = "no";
        transparency = 0;
        notification_height = 0;
        separator_height = 0;
        padding = 12;
        horizontal_padding = 16;
        corner_radius = 16;
        frame_width = 1;
        frame_color = "#8aadf4"; # blue
        separator_color = "frame";

        sort = "yes";
        idle_threshold = 120;

        font = "Inter 11";
        line_height = 0;
        markup = "full";
        format = ''<b>%s</b>\n%b'';
        alignment = "left";
        show_age_threshold = 60;
        word_wrap = "yes";
        ellipsize = "middle";
        ignore_newline = "no";
        stack_duplicates = true;
        hide_duplicate_count = false;
        show_indicators = "yes";

        icon_position = "left";
        max_icon_size = 32;

        sticky_history = "yes";
        history_length = 20;

        # TODO: Verify this works.
        browser = "firefox --new-tab";

        always_run_script = true;
        title = "Dunst";
        class = "Dunst";
        startup_notification = true;
      };

      shortcuts = {
        # TODO: Get this working.
        history = "mod4+n";
      };

      urgency_low = {
        background  = "#181926";
        foreground  = "#939ab7"; # lighter
        highlight   = "#cad3f5";
        frame_color = "#eed49f"; # yellow
        timeout = 15;
      };
      urgency_normal = {
        background  = "#181926";
        foreground  = "#cad3f5";
        highlight   = "#cad3f5";
        timeout = 15;
      };
      urgency_critical = {
        background  = "#181926";
        foreground  = "#cad3f5";
        highlight   = "#cad3f5";
        frame_color = "#ed8796"; # red
        timeout = 0;
      };

      volume_display_tweaks = {
        icon = "audio-volume-high";
        format = "%s %b";
      };
    };
  };
}
