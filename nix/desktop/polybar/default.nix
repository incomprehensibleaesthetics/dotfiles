{ pkgs, ... }:

let
  # TODO: Get these from the configured theme.
  colors = {
    background       = "#000000";
    foreground       = "#cad3f5";
    foregroundSubtle = "#6e738d";
    highlight        = "#8aadf4";
  };

  height = 26;
in {
  services.polybar = {
    enable = true;

    # Somehow this option doesn't work. It is required though.
    script = "true";

    settings = {
      "bar/workspaces" = {
        width = 216;
        inherit height;
        offset.x = "50%:-108";
        radius.bottom = 16;
        background = colors.background;
        foreground = colors.foregroundSubtle;
        font = [ "FontAwesome:style=Regular" ];
        modules.center = "xworkspaces";
      };

      "bar/clock" = {
        width = 226;
        inherit height;
        offset.x = "100%:-226";
        offset.y = 0;
        module.margin = 2;
        radius.bottom.left = 16;
        background = colors.background;
        foreground = colors.foreground;
        font = [ "FontAwesome:style=Regular" "Inter:style=Regular" ];
        modules.center = "date time";
      };

      "bar/background" = {
        width = 2560;
        inherit height;
        background = colors.background;
        modules.center = "blank-text";
      };

      "module/date" = {
        type = "internal/date";
        format = "%{F${colors.foregroundSubtle}}  %{F-} <label>";
        label.text = "%date%";
        label.font = 1;
        date = "%d %b %Y";
      };

      "module/time" = {
        type = "internal/date";
        format = "%{F${colors.foregroundSubtle}} %{F-} <label>";
        label.text = "%time%";
        label.font = 1;
        time = "%H:%M";
      };

      "module/xworkspaces" = {
        type = "internal/xworkspaces";
        enable.click = true;
        label.active.padding = 1;
        label.empty.padding = 1;
        label.occupied.padding = 1;
        label.urgent.padding = 1;
        label.active.foreground = colors.highlight;
        label.active.text = "";
        label.empty.text = "";
        label.occupied.text = "";
        label.urgent.text = "";
      };

      "module/blank-text" = {
        type = "custom/text";
        content = " ";
      };
    };
  };

  systemd.user.services.polybar-clock = {
    Unit = {
      Description = "Bar that displays a date and time";
      PartOf = "graphical-session.target";
      After = "graphical-session-pre.target";
    };
    Service = {
      ExecStart = "${pkgs.polybar}/bin/polybar clock";
    };
  };

  systemd.user.services.polybar-workspaces = {
    Unit = {
      Description = "Bar that displays workspaces";
      PartOf = "graphical-session.target";
      After = "graphical-session-pre.target";
    };
    Service = {
      ExecStart = "${pkgs.polybar}/bin/polybar workspaces";
    };
  };

  systemd.user.services.polybar-background = {
    Unit = {
      Description = "Simple bar with no functionality";
      PartOf = "graphical-session.target";
      After = "graphical-session-pre.target";
    };
    Service = {
      ExecStart = "${pkgs.polybar}/bin/polybar background";
    };
  };
}
