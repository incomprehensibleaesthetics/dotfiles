{ config, lib, pkgs, ... }:

with builtins;
with lib;

let

  # clipmenu will use this since the theme cannot be configured.
  baseConfig = {
    font = "Inter 12";
    terminal = "${pkgs.kitty}/bin/kitty";
    cycle = false;

    extraConfig = {
      show-icons = true;

      # Enable single-click mouse activation.
      hover-select = true;
      me-accept-entry = "MousePrimary";
      me-select-entry = "";
    };

    theme = catppuccinMacchiatoTheme;
  };

  launcherOverrides = {
    config = {
      font = "Inter 14";
    };
    theme = {
      window   = { width = mkLiteral "400 px"; }; 
      listview = { lines = 6; };
      element-icon = {
        size = mkLiteral "32px";
      };
    };
  };

  powerOverrides = {
    config = {
      font = "Inter 14";
      cycle = true;
    };
    theme = {
      window   = { width = mkLiteral "240 px"; }; 
      listview = { lines = 3; };
      element-icon = {
        size = mkLiteral "32px";
      };
    };
  };

  catppuccinMacchiatoTheme = {
    "*" = {
      background-color = mkLiteral "#181926";
      border-color     = mkLiteral "#939ab7";
      text-color       = mkLiteral "#cad3f5";
      spacing = 0;
    };

    window = {
      border        = mkLiteral "1px";
      border-color  = mkLiteral "#ed8796"; # red
      border-radius = mkLiteral "16px";
      padding       = mkLiteral "16px";
      width         = mkLiteral "512px";
    };

    inputbar = {
      border     = mkLiteral "0 0 1px 0";
      padding    = mkLiteral "0 0 4px 0";
      margin     = mkLiteral "-8px 0 16px 0";
      text-color = mkLiteral "#cad3f5";
      children = [ "prompt" "entry" ];
    };

    prompt = {
      enabled = false;
    };

    entry = {
      padding = mkLiteral "8px 8px 8px 0";
    };

    listview = {
      lines = 8;
      margin = mkLiteral "-8px 0";
      scrollbar = false;
    };

    element = {
      border = 0;
      padding = mkLiteral "8px 0";
    };

    element-icon = {
      size = 0;
    };

    element-text = {
      padding = mkLiteral "5px 0 0 8px";

      # Required as of 1.7.0 to get the selected selected colors right.
      background-color = mkLiteral "inherit";
      text-color       = mkLiteral "inherit";
    };

    "element selected" = {
      "text-color" = mkLiteral "#8aadf4"; # blue
    };
  };

  inherit (config.lib.formats.rasi) mkLiteral;

  # TODO: Figure out a way to not have to copy-paste this code from rofi.nix:
  #
  # >>>

  mkValueString = value:
    if isBool value then
      if value then "true" else "false"
    else if isInt value then
      toString value
    else if (value._type or "") == "literal" then
      value.value
    else if isString value then
      ''"${value}"''
    else if isList value then
      "[ ${strings.concatStringsSep "," (map mkValueString value)} ]"
    else
      abort "Unhandled value type ${builtins.typeOf value}";

  mkKeyValue = { sep ? ": ", end ? ";" }:
    name: value:
    "${name}${sep}${mkValueString value}${end}";

  mkRasiSection = name: value:
    if isAttrs value then
      let
        toRasiKeyValue = generators.toKeyValue { mkKeyValue = mkKeyValue { }; };
        # Remove null values so the resulting config does not have empty lines
        configStr = toRasiKeyValue (filterAttrs (_: v: v != null) value);
      in ''
        ${name} {
        ${configStr}}
      ''
    else
      (mkKeyValue {
        sep = " ";
        end = "";
      } name value) + "\n";

  toRasi = attrs:
    concatStringsSep "\n" (concatMap (mapAttrsToList mkRasiSection) [
      (filterAttrs (n: _: n == "@theme") attrs)
      (filterAttrs (n: _: n == "@import") attrs)
      (removeAttrs attrs [ "@theme" "@import" ])
    ]);

  # <<<

in {

  programs.rofi = baseConfig // { enable = true; };

  xdg.configFile."rofi/config-launcher.rasi".text = toRasi {
    configuration = {
      inherit (baseConfig) font;
    } // baseConfig.extraConfig // launcherOverrides.config;
  } + toRasi { "@theme" = "custom"; } + toRasi launcherOverrides.theme;

  xdg.configFile."rofi/config-power.rasi".text = toRasi {
    configuration = {
      inherit (baseConfig) font;
    } // baseConfig.extraConfig // powerOverrides.config;
  } + toRasi { "@theme" = "custom"; } + toRasi powerOverrides.theme;

}
