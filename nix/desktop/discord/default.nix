{ ... }:

{
  xdg.configFile = {
    discord.source = ./settings.json;
    discord.target = "discord/settings.json";
  };
}
