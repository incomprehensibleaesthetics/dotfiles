args@{ config, lib, pkgs, ... }:

with pkgs;
with lib.attrsets;

let

  desktopCfg = attrByPath [ "nixosConfig" "myConfig" "desktop" ] { enable = false; } args;

in {
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "josh";
  home.homeDirectory = "/home/josh";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.03";

  imports = [
    ./desktop
    ./email
    ./helix
    ./lazygit
    ./theme
  ];


  ############################################################################

  # NixOS integration
  #
  # TODO: Migrate this.
  home.file.".oh-my-zsh/custom/nixos.zsh" = lib.mkIf (builtins.pathExists /etc/oh-my-zsh/nixos.zsh) {
    source = config.lib.file.mkOutOfStoreSymlink "/etc/oh-my-zsh/nixos.zsh";
  };


  ## General Config ##########################################################

  xdg.configFile = {
    mimi.source = mimi/mime.conf;
    mimi.target = "mimi/mime.conf";
  };


  ## Theme ###################################################################

  myConfig.desktop = optionalAttrs desktopCfg.enable (with desktopCfg; {
    inherit enable cursorTheme;
  });

  # TODO: Keep this in sync with nixos config.
  myConfig.theme.colorScheme = "catppuccin";
  # "frappe", "latte", "macchiato" or "mocha"
  myConfig.theme.colorSchemeFlavor = "macchiato";


  ## Packages / Programs #####################################################

  programs.helix.enable = true;

  # TODO: Add icons file.
  programs.lf = {
    enable = true;

    keybindings = {
      "gd" = "cd ~/inbox/downloads/";
      "gi" = "cd ~/inbox/";
      "gp" = "cd ~/resources/paperwork/by-organisation/";
      "gw" = "cd ~/areas/work/";

      "ge" = "bottom";

      "oa" = "set sortby atime";

      "<delete>" = "delete";
    };

    commands = {
      "delete" = "$trash $fx";
    };

    # TODO: Only enable this when the desktop is enabled?
    previewer.source = pkgs.writeShellScript "lf_kitty_preview.sh" ''
      file=$1
      w=$2
      h=$3
      x=$4
      y=$5
      hfloat=$(expr $h*0.9 | bc)
      hint=''${hfloat/.*}
      CACHE="$HOME/.cache/tmpimg"

      # TODO: Figure out why this line causes everything to go to shit.
      #
      # kitty +icat --silent --clear

      if [[ "$(file -Lb --mime-type "$file")" =~ ^image ]]; then
        kitty +icat --silent --transfer-mode file --place "''${w}x''${h}@''${x}x2" "$file"
        exit 0
      fi

      if [[ "$(file -Lb --mime-type "$file")" =~ ^application/pdf ]]; then
        pdftoppm -jpeg -f 1 -singlefile "$file" "$CACHE"
        kitty +icat --silent --transfer-mode file --place "''${w}x$hint@''${x}x2" "''${CACHE}.jpg"
        rm "''${CACHE}.jpg"
        exit 0
      fi

      cat "$file"
    '';

    extraConfig = (
      let
        cleaner_script = pkgs.writeShellScript "lf_kitty_cleaner.sh" ''
          kitty +icat --clear --silent --transfer-mode file
        '';
      in ''
        set cleaner ${cleaner_script}
      ''
    );

    settings = {
      icons = true;
      ifs = "\n";
    };
  };

  programs.ncmpcpp = {
    enable = true;

    settings = {
      mpd_host = "clio";
      mpd_music_dir = "/srv/music";
    };

    bindings = [
      { key = "k"; command = "scroll_up"; }
      { key = "j"; command = "scroll_down"; }
      { key = "K"; command = [ "select_item" "scroll_up" ]; }
      { key = "J"; command = [ "select_item" "scroll_down" ]; }
      { key = "alt-k"; command = "move_selected_items_up"; }
      { key = "alt-k"; command = "move_sort_order_up"; }
      { key = "alt-j"; command = "move_selected_items_down"; }
      { key = "alt-j"; command = "move_sort_order_down"; }

      { key = "h"; command = "previous_column"; }
      { key = "l"; command = "next_column"; }

      { key = "n"; command = "next_found_item"; }
      { key = "alt-n"; command = "previous_found_item"; }
    ];
  };
}
