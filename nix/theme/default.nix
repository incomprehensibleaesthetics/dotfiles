{ config, lib, ... }:

with lib;

{
  options.myConfig.theme = {
    colorScheme = mkOption {
      type = types.str;
      example = "catppuccin";
      description = ''
        Name of the color scheme to use.
      '';
    };

    colorSchemeFlavor = mkOption {
      type = types.str;
      example = "frappe";
      description = ''
        Name of the color scheme flavor to use.
      '';
    };
  };
}
