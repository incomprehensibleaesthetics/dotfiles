{ config, ... }:

let
  scheme = config.myConfig.theme.colorScheme;
  flavor = config.myConfig.theme.colorSchemeFlavor;
in {
  programs.helix = {
    settings = {
      theme = "${scheme}_${flavor}";

      editor = {
        color-modes = true;
        cursorline = true;
        idle-timeout = 0; # Don't wait before doing auto-complete.
        rulers = [ 80 98 ];
        scrolloff = 7;
        soft-wrap.enable = true;
      };

      editor.cursor-shape = {
        insert = "bar";
      };

      editor.file-picker = {
        # Symlinks can cause extreme lag when there is a `result` symlink in the
        # working directory.
        follow-symlinks = false;
      };

      editor.statusline = {
        # TODO: Figure out why the spinner is invisible.
        left = [ "mode" "spinner" "file-modification-indicator" "file-name" "separator" "spacer" "version-control" ];
        right = [ "diagnostics" "selections" "position" "total-line-numbers" "position-percentage" ];
      };

      editor.whitespace.render = {
        tab = "all";
      };

      keys.normal = {
        y = [ "yank" ":clipboard-yank" ];
        "C-A-j" = [ "extend_to_line_bounds" "delete_selection" "paste_after" ];
        "C-A-k" = [ "extend_to_line_bounds" "delete_selection" "move_line_up" "paste_before" ];
      };

      keys.normal."C-A-u" = {
        r = ":reflow";
      };
    };

    languages = {
      language = [
        {
          name = "elixir";
          formatter = { command = "mix"; args = [ "format" "-" ]; };
          auto-format = true;
        }
        {
          name = "nix";
          auto-pairs = {
            "=" = ";";
          };
        }
      ];
    };
  };
}
